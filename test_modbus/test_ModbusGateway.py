#!/usr/bin/env python

# This script can be used to test Teltonika Modbus Gateway
# Connect the Teltonika device and enable the Modbus Gateway feature
# Reference link --> https://wiki.teltonika-networks.com/view/RUT955_RS232/RS485#Modbus_gateway
# It uses READING HOLDING REGISTERS to check the Teltonika parameters
# Take a look to the README file to install and use diagslave as modbus simulator

from pymodbus.client.sync import ModbusTcpClient
from datetime import datetime, date
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
#from pymodbus.compat import iteritems
from time import sleep

# Teltonika slave device IP
host = '192.168.1.1'
# Teltonika slave device Port
port = 8000
# Teltonika slave ID
slave_id = 1
# First register in modbus table
first_request = 0
# Quantity of registers in modbus table
register_count = 5
# Time to recall modbus request
loop_time = 5
# Function to get time and date
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
today = date.today().strftime("%B %d, %Y")

# Function to read_holding_registers
def read_holding_registers(result):
    result = client.read_holding_registers(first_request, register_count, unit=slave_id)
    print(f'  {result.registers}')
    client.close()
    return

# More functions to be added
#
#
#
#
#
#
#
#
#


try:
    x = 0
    while True:
        #Request ID counter
        x += 1
        # Connecting to the device
        client = ModbusTcpClient(host, port)
        print(f'\n "Reading holding registers from {host} and port {port}" at {today} {current_time}')
        print(f' - Request {x}')
        read_holding_registers(client)
        # Time between requests
        sleep(loop_time)

# Managing errors
except Exception as e:
    print("Error:")
    print(e)