#!/usr/bin/env python

# This script can be used to test Teltonika devices as TCP slaves.
# Connect the Teltonika device and enable the TCP slave feature
# This Guide can explain what I did --> https://wiki.teltonika-networks.com/view/Monitoring_via_Modbus
# It uses READING HOLDING REGISTERS to check the Teltonika parameters
# Take a look to the README file to install and use diagslave as modbus simulator

from pymodbus.client.sync import ModbusTcpClient
from datetime import datetime, date
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
from pymodbus.compat import iteritems
from time import sleep

# Teltonika slave device IP
host = '192.168.1.1'
# Teltonika slave device Port
port = 502
# Teltonika slave ID
slave_id = 2
# Time to recall modbus request
request_time = 5
# Function to get time and date
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
today = date.today().strftime("%B %d, %Y")

# Function to get and parse device name
def deviceName(result):
    result = client.read_holding_registers(7, 16, unit=slave_id)
    decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Big)
    decoded = {
        ' - Device Name': decoder.decode_string(16).decode(),
    }
    for name, value in iteritems(decoded):
        print ("%s\t" % name, value)
    client.close()
    return

# Function to get device's time up and parse it to minutes
def timeUP(result):
    result = client.read_holding_registers(2, 2, unit=slave_id)
    decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Big)
    decoded = {
        'Time Up': decoder.decode_16bit_uint()/60,
    }

    for name, value in iteritems(decoded):
        hour = value / 60
        if value > 59.99:
            print(f' - {name}: {"%.2f" % hour} hours')
        else:         
            print(f' - {name}: {"%.2f" % value} minutes')
    client.close()

    return

# More functions to be added
#
#
#
#
#
#
#
#
#

try:
    x = 0
    while True:
        #Request ID counter
        x += 1
        # Connecting to the device
        client = ModbusTcpClient(host, port)
        print(f'\n "Reading holding registers from {host} and port {port}" at {today} {current_time}')
        print(f' - Request {x}')
        # Getting Device's Name
        deviceName(client)     
        # Getting Time UP   
        timeUP(client)
        # Time between requests
        sleep(request_time)

# Managing errors
except Exception as e:
    print("Error:")
    print(e)