# This script can be used to test the modbus data to server service in Teltonika devices
# Connect the Teltonika device, a modbus slave or use Diagslave simulator.
# Set up Modbus Master in Teltonika device, create requests and also configure data to server (To your localhost)
#
#
#     -----------------                   -----------------                    -----------------
#    |  Modbus device  |     USB to      |    Teltonika    |   USB cable or   |                 |               
#    |   or PC with    |   RS232/RS485   |     Device      |     Ethernet     |    PC Server    |
#    |   diagslave     |-----------------|    RUT or TRB   | -----------------|                 |
#    |   Simulator     | RS485/RS232 I/O |                 |                  |                 |
#     -----------------                   -----------------                    -----------------
#
# Reference links:
# https://wiki.teltonika-networks.com/view/RUT955_Modbus#Modbus_TCP_Master
# https://wiki.teltonika-networks.com/view/RUT955_Modbus#Modbus_Serial_Master
# https://wiki.teltonika-networks.com/view/RUT955_Modbus#Modbus_Data_to_Server
# Take a look to the README file to install and use diagslave as modbus simulator

from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import json
import sqlite3
from sqlite3 import Error


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Local Database name
db_name = 'db_from_http_requests'
# Quantity of request should receive the script to compare with Teltonika database.
requests = 100
# RUT/TRB that will be tested 
ip = '192.168.2.1'
listen_ip = '0.0.0.0'
# Port will be enabled in this server
port = 8000


# Functions to use sqlite
class db():
    def create_connection(db_file):
        """ create a database connection to a SQLite database """
        conn = None
        try:
            if not os.path.exists(db_file):
                print("Database doesn't exist. Creating new database...")          
            conn = sqlite3.connect(db_file)
        except Error as e:
            print(e)
        return conn

    def create_table(conn):
        sql = """ CREATE TABLE IF NOT EXISTS modbus_data (
                                    id integer PRIMARY KEY,
                                    epoch_time TEXT,
                                    modbus_registers TEXT
                                ); """
        try:
            c = conn.cursor()
            c.execute(sql)
        except Error as e:
            print(e)

    def create_modbus_data(conn, modbus_data):
        sql = ''' INSERT INTO modbus_data(epoch_time,modbus_registers)
                VALUES(?,?) '''
        cur = conn.cursor()
        cur.execute(sql, modbus_data)
        conn.commit()
        return cur.lastrowid

    def compare_data():
        # Getting both databases
        db_local = os.path.join(BASE_DIR, db_name)
        db_teltonika = os.path.join(BASE_DIR + '/.modbus_tcp_master_db')
        x = False
        equal_count = 0
        notequal_count = 0
        for i in range(1, requests):
            
            #Getting data from Local database
            if x == False:
                print('Connecting to DB local')
            conn_local = db.create_connection(db_local)
            cur = conn_local.cursor()
            cur.execute(f"SELECT epoch_time FROM modbus_data WHERE id = {i}")
            value_local = cur.fetchall()
            for value_l in value_local:
                for vl in value_l:
                    vl = int(vl)

            #Getting data from Teltonika database
            if x == False:
                x = True
                print('Connecting to DB Teltonika')
            conn_teltonika = db.create_connection(db_teltonika)
            cur = conn_teltonika.cursor()
            cur.execute(f"SELECT time FROM modbus_data WHERE id = {i}")
            value_teltonika = cur.fetchall()
            for value_t in value_teltonika:
                for vt in value_t:
                    vt = vt

            # Comparing both databases
            if vl == vt:
                equal = 'Equal'
                equal_count += 1
            else:
                equal = 'There is something wrong Here. Remove current databases, reboot Teltonika device and check again'
                notequal_count += 1

            # Printing results
            if i in range(1, 10):
                print(f'''
                    Row {i}   |     Local     |    Teltonika    |   Is equal?
                            |  {vl}   |   {vt}    |   {equal}
                    ''')

            if i in range(11, 100):
                print(f'''
                    Row {i}  |     Local     |    Teltonika    |   Is equal?
                            |  {vl}   |   {vt}    |   {equal}
                    ''')
    
            if i in range(101, 1000):
                print(f'''
                    Row {i} |     Local     |    Teltonika    |   Is equal?
                            |  {vl}   |   {vt}    |   {equal}
                    ''')
        print(f'''
            Equals: {equal_count}
            Not equals: {notequal_count}
        ''')

# Server handler
class handler(BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
    
    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        self._set_headers()
        message = "Hello, World! Here is a GET response"
        self.wfile.write(bytes(message, "utf8"))
        
    def do_POST(self):
        self._set_headers()
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        
        try:
            
            json_obj = json.loads(self.data_string)
            # Appending requests to a single fil            
            with open("json_requests.txt", "a") as myfile:
                myfile.write(str(json_obj) + '\n')

            # Connecting to database or creating if case it doesn't exists
            conn = db.create_connection(os.path.join(BASE_DIR, db_name))
            # if there is a database connection:
            if conn is not None:
                db.create_table(conn)
                # Inserting data to database            
                modbus_data = (f"{json_obj['TS']}", f"{json_obj['data']}")
                # Checking last ID in database
                modbus_data_id = db.create_modbus_data(conn, modbus_data)
                print(f"Request {modbus_data_id} from {ip} added to database successfully")
                # Achieved number of requests
                if modbus_data_id > requests:
                    server.server_close()
                    #Check if Teltonika database exists
                    if not os.path.isfile(BASE_DIR + '/.modbus_tcp_master_db'):
                        print(f"""
                            Ready, you've received {modbus_data_id} requests.
                            Now you should download the database via scp
                            from your Teltonika device
                            
                            Please insert your RUT/TRB password                
                            """)
                        # Downloading modbus database from RUT/TRB
                        os.system(f'scp root@{ip}:/tmp/.modbus_tcp_master_db .')
                        print("Downloading Teltonika database")
                    else:
                        print('Database already downloaded')
                    db.compare_data()

            else:
                print("Error! cannot create the database connection.")


        except Exception as e:
            print(e)

if __name__ == '__main__':
    try:
            with HTTPServer((listen_ip, port), handler) as server:
                print(f"Listening on localhost {port}")
                server.serve_forever()

    except Exception as e:
        print(e)