# Pymodbus - Teltonika devices

Requirements to use these scripts.

Install virtual enviroment
> virtualenv .venv

Access to the new virtual enviroment
> source .venv/bin/activate

Install requirements.txt
>pip install requirements.txt

Run diagslave as "Serial simulator"
Check all the diagslave options in:
> diagslave -h

Command example:
> sudo diagslave -m rtu -a 1 -b 9600 -d 8 -s 1 -p even /dev/ttyUSB0

Call each function using
> python the_test_you_want.py

If you're having a lot of non-equals values:

Stop Diagslave.
remove local databases.
Reboot Teltonika device.
Run Diagslave again.

Remember to flush dhcplient when reboot or pull out the USB cable

> sudo dhclient -r <the_USB_interface>